# Description
Un prototype de jeu du type "Side scroller shoot them up", écrit en blueprint et utilisant des assets low poly.

# Objectifs
Détruire tout le monde et survivre le plus longtemps.

# Mouvements et actions du joueur
Quitter le jeu: touche ESC suivi de clic gauche sur la croix, ou clic gauche sur la fenêtre suivi des touches alt+F4.
Se déplacer: touches WASD ou bien les flèches du clavier.
Tir principal : bouton gauche de la souris ou touche C.
Tir secondaire : bouton droit de la souris ou touche X.

# Interactions
Le joueur peut tirer des projectiles, détruire les ennemis et ramasser des objets.
Le joueur peut être endommagé par les ennemis et leurs projectiles.

# Copyrights
Tiré du tutoriel: "Unreal Engine 4 - Learn to Make a Game Prototype in UE4", disponible sur http://www.udemy.com

------------------
(C) 2017 Steamlead (www.steamlead.com)

==================================================================================================

# Description
A game prototype of the "Side scroller shoot them up" type, written in blueprint and using low poly assets.

# Targets
Destroy everyone and survive the longest.

# Movements and actions of the player
Quit the game: ESC key followed by left clicking on the cross, or left click on the window followed by alt + F4.
Move: WASD or the arrow keys.
Main fire: left mouse button or C key.
Secondary fire: right mouse button or X key.

# Interactions
The player can fire projectiles, destroy enemies and pick up items.
The player can be damaged by enemies and their projectiles.

# Copyrights
Taken for the tutorial: "Unreal Engine 4 - Learn to Make a Game Prototype in UE4", available on http://www.udemy.com

------------------
(C) 2017 Steamlead (www.steamlead.com)